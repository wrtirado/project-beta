import React, { useState, useEffect } from "react"
import { FetchWrapper } from "./fetch-wrapper"

function AddAppointmentForm() {
    const [allTechnicians, setAllTechnicians] = useState([])
    const [vin, setVIN] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')

    const ServiceApi = new FetchWrapper ('http://localhost:8080/')

    const fetchData = async () => {
        const technicianData = await ServiceApi.get('api/technicians/')
        setAllTechnicians(technicianData.Technicians)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}

        body.vin = vin
        body.customer = customer
        body.date_time = date
        body.technician = technician
        body.reason = reason
        body.status = "created"

        const newAppointment = await ServiceApi.post('api/appointments/', body)
        setVIN('')
        setCustomer('')
        setDate('')
        setTechnician('')
        setReason('')
    }

    const handleVINChange = (event) => {
        const value = event.target.value
        setVIN(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleDateChange = (event) => {
        const dateValue = event.target.value
        setDate(dateValue)
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleVINChange} placeholder="Automobile VIN..." required type="text" value={vin} name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                                <input onChange={handleCustomerChange} placeholder="Customer..." required type="text" value={customer} name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer-name">Customer Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} placeholder="Date and Time" required type="datetime-local" value={date} name="date" id="date" className="form-control" />
                            <label htmlFor="date-time">Appointment Date and Time...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleTechnicianChange} required value={technician} name="technician" id="technician" className="form-select">
                                <option value="">Choose a technician...</option>
                                {allTechnicians.map(Technicians => {
                                    return(
                                        <option key={Technicians.id} value={Technicians.id}>
                                            {Technicians.first_name} {Technicians.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                                <input onChange={handleReasonChange} placeholder="Reason..." required type="text" value={reason} name="reason" id="reason" className="form-control" />
                                <label htmlFor="reason">Reason...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddAppointmentForm
