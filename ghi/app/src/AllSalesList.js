import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function AllSalesList() {
    const [allSales, setAllSales] = useState([])

    const SalesAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const salesData = await SalesAPI.get('api/sales/')
        setAllSales(salesData.sales)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {allSales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.employee_id }</td>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default AllSalesList
