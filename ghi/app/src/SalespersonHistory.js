import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function SalespersonHistory() {
    const [allSalespeople, setAllSalespeople] = useState([])
    const [salesData, setSalesData] = useState([])
    const [individualSalesData, setIndividualSalesData] = useState([])
    const [select, setSelect] = useState('')

    const SalesAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const salespeopleData = await SalesAPI.get('api/salespeople/')
        const salesData = await SalesAPI.get('api/sales/')
        setAllSalespeople(salespeopleData.salespeople)
        setSalesData(salesData.sales)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSelect(value)
        setIndividualSalesData([...salesData].filter(sale => {
            return sale.salesperson.id === Number(value)
        }))
    }

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                    <div className="mb-3">
                            <select onChange={handleSalespersonChange} value={select} name="select" id="select" className="form-select">
                                <option value="">Choose a salesperson...</option>
                                {allSalespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                </div>
            </div>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {individualSalesData.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default SalespersonHistory
