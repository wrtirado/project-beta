import React, { useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function AddManufacturerForm() {
    const [name, setName] = useState('')

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}
        body.name = name
        const newManufacturer = await InventoryAPI.post('api/manufacturers/', body)
        setName('')
    }
    
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }


    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Manufacturer name..." required type="text" value={name} name="name" id="name" className="form-control" />
                            <label htmlFor="name">Manufacturer name...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddManufacturerForm
