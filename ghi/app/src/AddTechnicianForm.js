import React, { useState } from "react"
import { FetchWrapper } from "./fetch-wrapper"

function AddTechnicianForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeId, setEmployeeId] = useState('')

    const ServiceApi = new FetchWrapper ('http://localhost:8080/')

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}

        body.first_name = firstName
        body.last_name = lastName
        body.employee_id = employeeId

        const newTechnician = await ServiceApi.post('api/technicians/', body)
        setFirstName('')
        setLastName('')
        setEmployeeId('')
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)

    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleFirstNameChange} placeholder="First Name..." required type="text" value={firstName} name="first-name" id="first-name" className="form-control" />
                                <label htmlFor="first-name">First Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange} placeholder="Last Name..." required type="text" value={lastName} name="last-name" id="last-name" className="form-control" />
                                <label htmlFor="last-name">Last Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                                <input onChange={handleEmployeeIdChange} placeholder="Employee ID..." required type="text" value={employeeId} name="employee-id" id="employee-id" className="form-control" />
                                <label htmlFor="employee-id">Employee ID...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddTechnicianForm
