import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function AddSalespersonForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const SalespersonAPI = new FetchWrapper('http://localhost:8090/')

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}
        body.first_name = firstName
        body.last_name = lastName
        body.employee_id = employeeID
        const newSalesperson = await SalespersonAPI.post('api/salespeople/', body)
        setFirstName('')
        setLastName('')
        setEmployeeID('')
    }
    
    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value
        setEmployeeID(value)
    }
    

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="First name..." required type="text" value={firstName} name="first-name" id="first-name" className="form-control" />
                            <label htmlFor="first-name">First name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} placeholder="Last name..." required type="text" value={lastName} name="last-name" id="last-name" className="form-control" />
                            <label htmlFor="last-name">Last name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIDChange} placeholder="Employee ID..." required type="text" value={employeeID} name="employee-id" id="employee-id" className="form-control" />
                            <label htmlFor="employee-id">Employee ID...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddSalespersonForm
