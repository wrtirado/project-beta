import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import AutoModelsList from './AutoModelsList';
import SalespersonList from './SalespeopleList';
import AddSalespersonForm from './AddSalespersonForm';
import AddCustomerForm from './AddCustomerForm';
import CustomerList from './CustomerList';
import RecordNewSale from './RecordNewSale';
import AllSalesList from './AllSalesList';
import SalespersonHistory from './SalespersonHistory';
import AddManufacturerForm from './AddManufacturerForm';
import AutomobileList from './AutomobileList';
import AddModelForm from './AddModelForm';
import AddAutomobileForm from './AddAutomobileForm';
import AddTechnicianForm from './AddTechnicianForm';
import TechnicianList from './TechnicianList';
import AllCurrentServicesList from './AllCurrentServicesList';
import ServiceHistory from './AllServiceHistory';
import AddAppointmentForm from './AddServiceAppointment';


function App() {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="manufacturers">
                        <Route index element={<ManufacturerList />} />
                        <Route path="new" element={<AddManufacturerForm />} />
                    </Route>
                    <Route path="models">
                        <Route index element={<AutoModelsList />} />
                        <Route path="new" element={<AddModelForm />} />
                    </Route>
                    <Route path="automobiles">
                        <Route index element={<AutomobileList />} />
                        <Route path="new" element={<AddAutomobileForm />} />
                    </Route>
                    <Route path="salespeople">
                        <Route index element={<SalespersonList />} />
                        <Route path="new" element={<AddSalespersonForm />} />
                    </Route>
                    <Route path="customers">
                        <Route index element={<CustomerList />} />
                        <Route path="new" element={<AddCustomerForm />} />
                    </Route>
                    <Route path="sales">
                        <Route index element={<AllSalesList />} />
                        <Route path="new" element={<RecordNewSale />} />
                        <Route path="history" element={<SalespersonHistory />} />
                    </Route>
                    <Route path="appointments">
                        <Route index element={<AllCurrentServicesList />} />
                    <Route path="new" element={<AddAppointmentForm />} />
                        <Route path="history" element={<ServiceHistory />} />
                    </Route>
                    <Route path="technicians">
                        <Route index element={<TechnicianList />} />
                        <Route path="new" element={<AddTechnicianForm />} />
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
