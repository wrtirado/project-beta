# CarCar

Team:

* Will Tirado - Sales
* Ayram Deliz - Service

## Design

## Service microservice

There are three models that will be needed for the Service microservice. Technician, AutomobileVO, and Appointment. The service poller will need to be completed first so that it updates the AutomobileVO every 60 seconds with updated VINs from the Inventory service. After that is completed, work will begin on the models. the Technician model will need to be set up first since it will be a ForeignKey to the "technician" field in the Appointment model. Once all the models are set up, the views will be created, followed by the front-end once the models and views have passed their tests.


## Sales microservice

For starters, I plan on creating the requested AutomobileVO model in my microservice. With that created I will build out my poller to ensure that I am correctly pulling (polling? ;) ) data from the inventory microservice. This will ensure that my AutomobileVO model table will be populated with the most up to date data possible (within one minute or so of a new automobile's creation). Once that is up and running, I will create the other requested models. My reason for waiting for the poller to be set up is simply due to the fact that the Sale model will have a foreign key relationship to all other models in the microservice, including AutomobileVO. I want to test and make sure that everything is working with actual data once I get to building out my API View Functions.
